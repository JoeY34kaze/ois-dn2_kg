var dobiVzdevek;
var dobiKanal;
var vulgarneBesede = [];

function zamenjajSmejkote (sporocilo)
{
  var smejkoti = {
    ';)' : 'wink.png',
    ':)' : 'smiley.png',
    '(y)' : 'like.png',
    ':*' : 'kiss.png',
    ':(' : 'sad.png'
  }, url = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/";
  
  return sporocilo.replace(/[:;)(y*]+/g, function (match) {
    return typeof smejkoti[match] != 'undefined' ?
           '<img src="'+url+smejkoti[match]+'"/>' :
           match;
  });
}

function spremeniZnacke(sporocilo)
{
  return String(sporocilo).replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function divElementEnostavniTekst(sporocilo) {
  
  var sp = zamenjajSmejkote(spremeniZnacke(sporocilo));
  
  return $('<div style="font-weight: bold"></div>').html(sp);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = cenzuriraj(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text().substring($('#kanal').text().indexOf("@")+2).trim(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function cenzuriraj(besedilo) {
    for(var i = 0; i < vulgarneBesede.length; i++) {
        var index = besedilo.toLowerCase().indexOf(vulgarneBesede[i]);
        if(index !== -1) {
            var starred = vulgarneBesede[i].split('').map(function(c){return '*'}).join('');
            var newWord = besedilo.split('');
            newWord.splice(index, vulgarneBesede[i].length, starred);
            return newWord.join('');
        }
    }
    return besedilo;
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  $.get('swearWords.txt', function(data) {
  vulgarneBesede = data.split("\n");
  }, 'text'); 
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      dobiVzdevek = rezultat.vzdevek;
      sporocilo = 'Prijavljen si kot ' + dobiVzdevek + '.';
      $('#kanal').text(dobiVzdevek + " @ " + rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    dobiKanal = rezultat.kanal;
    $('#kanal').text(dobiVzdevek + " @ " + dobiKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  socket.on('zasebnoSporociloZahteva',function(sporocilo) {
       $('#sporocila').append(divElementEnostavniTekst(sporocilo));
  });

  socket.on('sporocilo', function (sporocilo) {
    var txt = "";
    
    $("<div>" + sporocilo.besedilo + "</div>").each(function() {
      txt = $(this).text();
    });
  
    var novElement = $('<div style="font-weight: bold"></div>').html(zamenjajSmejkote(txt));
    $('#sporocila').append(novElement);
  });

  socket.on('uporabniki', function (uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    for (var uID in uporabniki) {
        //if (uporabniki != null)
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[uID]));
    }
  });
  
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);
  
  /*setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);*/

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});