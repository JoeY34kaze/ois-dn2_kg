var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      // /zasebno "Gost1" "Hej, kako si?"
      besede.shift(); // da se znebimo /zasebno
      besede = besede.join(" "); // damo v en String, imamo (presledek)"Gost1" "Hej, kako si?"
      besede = besede.trim(); // "Gost1" "Hej, kako si?"
      
      besede = splitter(besede); // razdeli na "Gost1" in "Hej, kako si?" (brez ")
      this.socket.emit('zasebnoSporociloZahteva', besede[0], besede[1]);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

function splitter(str) {
    return str.split('"').filter(function(w){ 
        return w.split(' ').join('')
    })
}